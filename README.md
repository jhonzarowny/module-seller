# Module Seller

## Description

This is a Sample Magento 2 Module, that means is not for production, the main purpose of this module is to demonstrate the ability to work with Magento and PHP.

This module can be installed via composer, this is just for demo purpose, I will remove after.

```bash
$ composer require jhonzarowny/module-seller:dev-master
```

## Features

### Endpoints
- GET `/rest/V1/sellers` - Will return a list of Sellers 
- GET `/rest/V1/sellers/:sellerId` - Will return a specific seller
- POST `/rest/V1/sellers` - Will create a new seller

#### GET `/rest/V1/sellers`
Request:
```bash
$ curl --location --request GET 'http://dev.magento23ce/rest/V1/sellers'
```
Response:
```json
{
    "items": [
        {
            "id": 403,
            "name": "Sample Seller 403"
        },
        {
            "id": 345,
            "name": "Sample Seller 345"
        },
        ...
    ]
}
```

#### GET `/rest/V1/sellers/:sellerId`
Request:
```bash
$ curl --location --request GET 'http://dev.magento23ce/rest/V1/sellers/222'
```
Response:
```json
{
    "id": 222,
    "name": "Sample Seller 222"
}
```


#### POST `/rest/V1/sellers`
Request:
```bash
$ curl --location --request POST 'http://dev.magento23ce/rest/V1/sellers' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  	"seller": {
  		"name": "Sample Seller"
  	}
  }'
```
Response:

```json
{
    "id": 520,
    "name": "Sample Seller 520"
}
```

## DTO interfaces

You can find the DTO interfaces of Magento modules in the [/Api/Data](/Api/Data) directory.

For this example I created the [SellerInterface](/Api/Data/SellerInterface.php) this DTO will store the `Seller` info.

## DTO implementation
You can find the DTO implementation of Magento modules in the [/Model](/Model) directory.

For this example I created the [Seller](/Model/Seller.php).

The Seller class in implementing `\Magento\Framework\DataObject` because we are not using persisting this DTO to MySQL table. 

In the Line 23 since we are not persisting this to the database, I'm using PHP rand to generate some Id's if it is null.
```php
public function getId()
{
    if (null == $this->id) {
        $this->id = rand(10, 1000);
    }
    return $this->id;
}
```

## service's contract interfaces

You can find the [service contracts](https://devdocs.magento.com/guides/v2.3/extension-dev-guide/service-contracts/service-contracts.html) we use here [/Api](/Api) directory.

For this example I created the [SellerManagementInterface](/Api/SellerManagementInterface.php) this service contract 
is responsible for seller's operations like `get`, `save`, `getList`.


## service's implementation
You can find the service's implementation of Magento modules in the [/Model](/Model) directory.

For this example I created the [SellerManagement](/Model/SellerManagement.php).

In the line 76, I create a fakeSellers method to mock some seller's data
```php
private function fakeSellers()
{
    $sellersDummy = [];
    for ($i=0;$i <= 100;$i++) {
        $seller = $this->sellerFactory->create();
        $seller->setName("Sample Seller {$seller->getId()}");
        $sellersDummy[] = $seller;
    }
    return $sellersDummy;
}
```

For testing purpose I added some `$this->logger->info("");`, in real modules, should be created a Enable/Disable log feature on the Magento admin panel, so the store owner or developer can choose save logs on `/var/logs` directory or not.

## ok, but how magento's know what to do with GET `/rest/V1/sellers`

Magento 2.x provide to us a wrapper to REST api functionality, the only thing we have to so is configure in the [/etc/webapi.xml](/etc/webapi.xml) the route, the desired service contract and method.
