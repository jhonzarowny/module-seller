<?php

namespace JhonZarowny\Seller\Model;

/**
 * Seller.
 */
class Seller extends \Magento\Framework\DataObject implements \JhonZarowny\Seller\Api\Data\SellerInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        if (null == $this->id) {
            $this->id = rand(10, 1000);
        }

        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
