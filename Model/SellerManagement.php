<?php

namespace JhonZarowny\Seller\Model;

/**
 * Class SellerManagement.
 */
class SellerManagement implements \JhonZarowny\Seller\Api\SellerManagementInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var SellerFactory
     */
    protected $sellerFactory;

    /**
     * @var sellerSearchResultsFactory
     */
    protected $sellerSearchResultsFactory;

    /**
     * SellerManagement constructor.
     *
     * @param \Psr\Log\LoggerInterface   $logger
     * @param SellerFactory              $sellerFactory
     * @param SellerSearchResultsFactory $sellerSearchResultsFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \JhonZarowny\Seller\Model\SellerFactory $sellerFactory,
        \JhonZarowny\Seller\Model\SellerSearchResultsFactory $sellerSearchResultsFactory
    ) {
        $this->logger = $logger;
        $this->sellerFactory = $sellerFactory;
        $this->sellerSearchResultsFactory = $sellerSearchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function get($sellerId)
    {
        $this->logger->info("getting seller {$sellerId}");
        $seller = $this->sellerFactory->create();
        $seller->setId($sellerId);
        $seller->setName("Sample Seller {$sellerId}");

        return $seller;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\JhonZarowny\Seller\Api\Data\SellerInterface $seller)
    {
        $seller->setId(rand(10, 1000));
        $seller->setName("{$seller->getName()} {$seller->getId()}");
        $this->logger->info("saving seller {$seller->getName()}");

        return $seller;
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        $sellers = $this->sellerSearchResultsFactory->create();
        $sellers->setItems($this->fakeSellers());
        $sellers->setTotalCount(count($this->fakeSellers()));
        $this->logger->info("Found {$sellers->getTotalCount()} sellers");

        return $sellers;
    }

    private function fakeSellers()
    {
        $sellersDummy = [];
        for ($i = 0; $i <= 100; ++$i) {
            $seller = $this->sellerFactory->create();
            $seller->setName("Sample Seller {$seller->getId()}");
            $sellersDummy[] = $seller;
        }

        return $sellersDummy;
    }
}
