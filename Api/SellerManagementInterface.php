<?php

namespace JhonZarowny\Seller\Api;

/**
 * Interface SellerManagementInterface.
 *
 * @api
 */
interface SellerManagementInterface
{
    /**
     * GET Seller by id.
     *
     * @param int $sellerId
     *
     * @return Data\SellerInterface
     */
    public function get($sellerId);

    /**
     * POST Seller.
     *
     * @param Data\SellerInterface $seller
     *
     * @return Data\SellerInterface
     */
    public function save(Data\SellerInterface $seller);

    /**
     * POST Seller.
     *
     * @return Data\SellerSearchResultsInterface
     */
    public function getList();
}
