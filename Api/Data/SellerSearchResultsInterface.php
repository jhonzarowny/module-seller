<?php

namespace JhonZarowny\Seller\Api\Data;

/**
 * Interface SellerSearchResultsInterface.
 *
 * @api
 */
interface SellerSearchResultsInterface
{
    /**
     * Get seller list.
     *
     * @return \JhonZarowny\Seller\Api\Data\SellerInterface[]
     */
    public function getItems();

    /**
     * Set seller list.
     *
     * @param \JhonZarowny\Seller\Api\Data\SellerInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
