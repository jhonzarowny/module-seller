<?php

namespace JhonZarowny\Seller\Api\Data;

/**
 * Interface SellerInterface.
 *
 * @api
 */
interface SellerInterface
{
    /**
     * Seller id.
     *
     * @return int
     */
    public function getId();

    /**
     * Set seller id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Seller name.
     *
     * @return string
     */
    public function getName();

    /**
     * Set seller name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name);
}
